# Huuuge Shell Utils
A set of shell scripts and utilities developed while working at Huuuge Games.

## Installation
Clone this repository, then move the contents of `Scripts` subdirectory
to one of the directories in your ${PATH}, like `/usr/local/bin`.
Alternatively you may opt for adding a Scripts directory to your ${PATH}
variable.

## Contents

### MagicCasino git helpers (a.k.a. GitWithStyle suite):
These tools allow you to quickly clone,
pull and build a MagicCasino repository.  
All the hard work is done automatically - you do not have to type
the commands yourself.  
The output from the commands is being printed with additional colorized info,
which includes the command currently executed, the timestamp etc.
While this is mostly an eye-candy, it can help the user spot which command
is being executed by the worker now and estimate how much time will elapse
until the entire commands chain is completed.
At the same time, the automated worker checks the sanity of both the input and
the environment variables before launching, prohibiting the use of the tool
in case of wrong setup. While running, the exitcodes of executed commands
are being monitored - should any command fail for whatever reason,
the worker tool will halt immediately. This ensures that there are no
silent intermediate errors in the chain of executed commands.  
#### GitWithStyleUtils
A set of functions related to pretty-printing and launching
(chains of) commands.

#### CloneMC
Clones the MagicCasino repository to a location selected by the user.
Then prepares the build for Win32 platform.
Requires: ssh-agent, git, make

#### PullMC
Updates an existing MagicCasino repository by pulling the changes
from upstream, updating the submodules, downloading new fat files etc.
Finally re-creates build files for Win32 platform, leaving the user
with an up-to-date copy of MagicCasino, ready to be built and launched.

#### MakeAndroid
Builds an existing MagicCasino repository for Android targets.
This includes building the resources and executing Gradle builds.
Finally sends the created .apk file to a target.

### Others:

#### git-whoami
git-whoami prints out your git username and git email to the standard output.
Very useful for managing different identities in distinct repositories.  
*Usage:* **git whoami** (in a Git repository)

#### IP
Prints out your current IPv4 address. To be run under Cygwin.

#### Ratio
Calculates and prints out the relative ratio of a set of numbers.
Useful for calculating the ratio of displays.  
*Usage:* **Ratio a b [c d ... n]**  
*Example:* **Ratio 1280 720** (prints out: `16:9 (scale factor: 80)`)

#### SLN
Opens an SLN file of a MagicCasino copy just by knowing its suffix.
For example, if you issued a `CloneMC alpha` command and wish to build it,
issue an additional `SLN alpha` command.
